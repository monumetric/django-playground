import requests
from django.conf import settings


def calculate_programs(sites: list):
    """Given a list of sites, calculate and assign the appropriate program
    for each site.

    Program tiers can be found here: https://www.monumetric.com/join/

    Args:
        sites (list): list of sites to assign programs, in this fomrat:
        [...
        {'id': 1559, 'name': 'site_url', 'program': None, 'pageviews': 289658},
        ...]
    """

    # Add logic here...
    data = sites

    return data


def upload_updated_values(sites: list):
    """Given a list of sites (the return value from calculate_programs),
    upload those values to the server.
    There is a slight hiccup, however- Occasionally the server will have what
    is called a "row lock error" that will prevent the bulk api from saving
    the entire provided payload. This function will need to account for that
    by manipulating the size, length, or any other attribute of the payload
    such that the bulk api is able to save all the updated records.

    Args:
        sites (list): list of sites to assign programs, in this fomrat:
        [...
        {'id': 1559, 'name': 'site_url', 'program': None, 'pageviews': 289658},
        ...]
    """

    # Add logic here to account for row lock errors...
    data = sites

    # Send entire site list once. You will likely need to call this with
    # varying payloads in order to get them all to save.
    requests.patch(f"{settings.REMOTE_SERVER}/server/accounts/bulk-update/",
                   headers={"Content-Type": "application/json"}, json=data)