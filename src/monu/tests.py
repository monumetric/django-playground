from django.test import TestCase
from django.conf import settings
import random
import requests
from .tasks import calculate_programs, upload_updated_values
from django.test.testcases import SerializeMixin

REMOTE_SERVER = settings.REMOTE_SERVER


class SiteUpdateTestCase(TestCase):
    def setUp(self):
        """Tell the server to generate a new set of sites ready for you to
        update.

        Raises:
            ValueError: Error to identify an issue with getting a set of
            sites to use from the server.
        """
        resp = requests.post(f'{REMOTE_SERVER}/server/prepare-environment')
        if not resp.status_code == 201:
            # If for some reason the server is unable to generate your list,
            # identify there's a problem.
            raise ValueError("ERROR SETTING UP TEST ENVIRONMENT. "
                             "Please contact Monumetric for assistance.")

    def test_program_assignment(self):
        """Validate that the proper programs have been assigned to each site
        """
        site_list = requests\
            .get(f'{REMOTE_SERVER}/server/accounts/').json()
        updated_sites = calculate_programs(site_list)
        answer_key = requests\
            .get(f'{REMOTE_SERVER}/server/accounts/?solutions=true').json()
        for site in answer_key:
            self.assertIn(site, updated_sites)

    def test_program_assignment_and_upload(self):
        """Validate that all updates have been saved on the server.
        (i.e. row lock errors have been successfully dealt with.)
        """
        site_list = requests.get(f'{REMOTE_SERVER}/server/accounts/').json()
        updated_sites = calculate_programs(site_list)
        site_update_resp = upload_updated_values(updated_sites)
        answer_key = requests\
            .get(f'{REMOTE_SERVER}/server/accounts/?solutions=true').json()
        user_saved_sites = requests\
            .get(f'{REMOTE_SERVER}/server/accounts/').json()

        api_calls = requests.get(f'{REMOTE_SERVER}/server/api-status')\
            .json()

        for site in answer_key:
            self.assertIn(site, user_saved_sites)

    def test_api_call_limit(self):
        """Bonus test- if you're able to get the sites to update using fewer
        API calls than there are sites, then you get a bonus point!
        """
        site_list = requests.get(f'{REMOTE_SERVER}/server/accounts/').json()
        updated_sites = calculate_programs(site_list)
        site_update_resp = upload_updated_values(updated_sites)
        answer_key = requests\
            .get(f'{REMOTE_SERVER}/server/accounts/?solutions=true').json()
        user_saved_sites = requests\
            .get(f'{REMOTE_SERVER}/server/accounts/').json()

        api_call_req = requests.get(f'{REMOTE_SERVER}/server/api-status')\
            .json()
        api_calls = api_call_req.get('request_count', 0)
        self.assertLess(api_calls, len(site_list))
